﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckConnection : MonoBehaviour
{
    public string m_url= "https://avatars1.githubusercontent.com/u/20149493?s=460&u=3f3c9e84bec37d51edfbb88659b0da1f2c58518e&v=4";
    public int m_delay=1;
    public ConnectionOnOff      m_lastState = new ConnectionOnOff();
    public ConnectionStateEvent m_onStateCheck = new ConnectionStateEvent();
    public ConnectionStateEvent m_onStateChange = new ConnectionStateEvent();
    public ConnectionRecoverEvent m_onConnectionrecover = new ConnectionRecoverEvent();
    public DisconnectionInfo m_disconnectionInfo=null;
    public double m_bytesDownloadedCount;

    [System.Serializable]
    public class ConnectionStateEvent : UnityEvent<ConnectionOnOff>
    {
    }


    [System.Serializable]
    public class ConnectionRecoverEvent : UnityEvent<DisconnectionInfo>
    {
    }
    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(m_delay);
            Debug.Log("Tik");
            StartCoroutine( CheckInternetConnection());
        }
        
        
    }

    private IEnumerator CheckInternetConnection()
    {
        ConnectionOnOff state = new ConnectionOnOff();

        state.StartRecord();
        WWW page = new WWW(m_url);
        yield return  page;
        m_bytesDownloadedCount += page.bytes.Length;
        state.StopRecord();
        if (page.error!=null &&page.error.Length > 0)
        {
            state.SetAsFailed();
        }
        else state.SetAsSucced();

        m_onStateCheck.Invoke(state); ;
        if (state.m_succedToRecover != m_lastState.m_succedToRecover) {

            if (state.m_succedToRecover)
            {
                if (m_disconnectionInfo != null) { 
                    m_disconnectionInfo.RecoverConnection();
                    m_onConnectionrecover.Invoke(m_disconnectionInfo);
                }
            }
            else {
                m_disconnectionInfo = new DisconnectionInfo();
                m_disconnectionInfo.LostConnection();
            }


            m_onStateChange.Invoke(state);

        }

        m_lastState = state;
    }

}
public class ConnectionOnOff
{
    public DateTime m_startRecoveringInfo;
    public DateTime m_stopRecoveringInfo;
    public bool m_succedToRecover;
    public double GetTimeToRecover() { return (m_stopRecoveringInfo - m_startRecoveringInfo).TotalSeconds; }
    public void SetAsSucced() { m_succedToRecover = true; }
    public void SetAsFailed() { m_succedToRecover = false; }
    public void StartRecord() { m_startRecoveringInfo = DateTime.Now; }
    public void StopRecord() { m_stopRecoveringInfo = DateTime.Now; }
}

public class DisconnectionInfo {
    public DateTime m_lostConnection;
    public DateTime m_recoveringConnection;
    public double GetDisconnectionLenght() { return (m_recoveringConnection - m_lostConnection).TotalSeconds; }
    public void LostConnection() { m_lostConnection = DateTime.Now; }
    public void RecoverConnection() { m_recoveringConnection = DateTime.Now; }

    
}
