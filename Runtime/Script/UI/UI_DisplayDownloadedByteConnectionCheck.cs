﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DisplayDownloadedByteConnectionCheck : MonoBehaviour
{
    public CheckConnection m_connection;
    public Text m_textToDisplay;
   
    void Update()
    {
        double byteCount = m_connection.m_bytesDownloadedCount;
         if (byteCount > 1073741824 )
            m_textToDisplay.text = string.Format("±{0:0.00 Go}", byteCount / 1073741824);
        else if (byteCount > 1048576)
            m_textToDisplay.text = string.Format("±{0:0.00 Mo}", byteCount / 1048576);
        else if (byteCount > 1024)
            m_textToDisplay.text = string.Format("±{0:0.00 ko}", byteCount / 1024);
        else 
            m_textToDisplay.text = string.Format("±{0:0.00 O}", byteCount);
    }
}
