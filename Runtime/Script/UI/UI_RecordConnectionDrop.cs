﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_RecordConnectionDrop : MonoBehaviour
{
    public Image [] m_connectionCheck;
    public double m_maxTime = 2;


    public void AddConnectionState(ConnectionOnOff onOff) {
        for (int i = m_connectionCheck.Length-1 ; i >0 ; i--)
        {
            m_connectionCheck[i].color      = m_connectionCheck[i-1].color;
            m_connectionCheck[i].fillAmount = m_connectionCheck[i-1].fillAmount;

        }
        m_connectionCheck[0].color = onOff.m_succedToRecover?Color.green:Color.red;
        m_connectionCheck[0].fillAmount = (float)(onOff.GetTimeToRecover() / m_maxTime);
    }

   
}
