﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetConnectionURL : MonoBehaviour
{
    public CheckConnection m_connection;
    public InputField m_inputLinked;
    public string m_prefsId = "URLSAVEINPREFSPLAYER";

    void Awake()
    {
        string saved = PlayerPrefs.GetString(m_prefsId, "").Trim();
        
        if (saved.Length > 0) {
            m_connection.m_url = saved;
            m_inputLinked.text = m_connection.m_url;
        }
    }

    public void ChangeURL(string newUrl) {
        m_connection.m_url = newUrl;
        PlayerPrefs.SetString(m_prefsId, m_connection.m_url);
    }


    void OnDisable()
    {
        PlayerPrefs.SetString(m_prefsId, m_connection.m_url);

    }
}
