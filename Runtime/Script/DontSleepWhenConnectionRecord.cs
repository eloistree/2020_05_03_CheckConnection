﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontSleepWhenConnectionRecord : MonoBehaviour
{
    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

}
