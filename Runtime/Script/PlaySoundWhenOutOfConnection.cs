﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundWhenOutOfConnection : MonoBehaviour
{
    public AudioSource m_player;
    public AudioClip m_playWhenLosingConnection;
    public AudioClip m_playOnReceivedFailedConnection;
    public AudioClip m_playWhenRecoverConnection;

    public void PlayBitWhenReceivedConnectionOff(ConnectionOnOff state)
    {
        if (!state.m_succedToRecover)
        {
            if(m_playOnReceivedFailedConnection)
            m_player.PlayOneShot(m_playOnReceivedFailedConnection);
        }
    }
    public void PlayOnConnectionChange(ConnectionOnOff state) {
        if (state.m_succedToRecover)
        {
            if (m_playWhenRecoverConnection)
                m_player.PlayOneShot(m_playWhenRecoverConnection);
        }
        else
        {
            if (m_playWhenLosingConnection)
                m_player.PlayOneShot(m_playWhenLosingConnection);
        }
    }
}
